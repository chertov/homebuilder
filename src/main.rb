
    require 'sketchup.rb'
    require_relative 'tools.rb'
    require_relative 'dialog.rb'
    require_relative 'observes.rb'
    require_relative 'tools/wall.rb'
    require_relative 'tools/roof.rb'
    require_relative 'tools/ray.rb'

    dialog_init

    module Examples
        module HelloCube

            # Here we add a menu item for the extension. Note that we again use a
            # load guard to prevent multiple menu items from accidentally being
            # created.
            unless file_loaded?(__FILE__)

                SKETCHUP_CONSOLE.show

                toolbar = UI::Toolbar.new "Cottage builder"

                cmd = UI::Command.new("Reload Code") { reloadPlugin }
                cmd.small_icon = "ToolPencilSmall.png"
                cmd.large_icon = "ToolPencilLarge.png"
                cmd.tooltip = "Test Toolbars"
                cmd.status_bar_text = "Testing the toolbars class"
                cmd.menu_text = "Test"
                toolbar = toolbar.add_item cmd

                cmd = UI::Command.new("Show Dialog") { dialog_show }
                cmd.small_icon = "ToolPencilSmall.png"
                cmd.large_icon = "ToolPencilLarge.png"
                cmd.tooltip = "Test Toolbars"
                cmd.status_bar_text = "Testing the toolbars class"
                cmd.menu_text = "Test"
                toolbar = toolbar.add_item cmd

                toolbar = toolbar.add_item ray_tool_init
                toolbar = toolbar.add_item tools_wall_init
                toolbar = toolbar.add_item roof_tool_init

                toolbar.show

                # menu = UI.menu('Plugins')
                # menu.add_item("Reload My Script") { reloadAll }

                file_loaded(__FILE__)
            end

        end # module HelloCube
    end # module Examples
