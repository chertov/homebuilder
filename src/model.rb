
    def modelJson(model)
        res = {}
        res['entities'] = entitiesJson model.entities,Geom::Transformation.new, true
        res['layers'] = entitiesJson model.layers
        res['name'] = model.name
        res['title'] = model.title
        res['activePath'] = entitiesJson model.active_path
        res['selection'] = entitiesJson model.selection
        # res['path'] = model.path
        res['guid'] = model.guid
        res['os_language'] = Sketchup.os_language
        return res
    end
