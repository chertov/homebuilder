
    require 'json'

    def vector3dJson(vec)
        return { 'x' => vec.x, 'y' => vec.y, 'z' => vec.z}
    end

    def entitiesJson(entities = nil, tr = Geom::Transformation.new, fullentity = false)
        return [] if entities == nil

        arr = []
        entities.map {|entity|
            ent = entityJson entity, tr, fullentity
            arr.push ent if ent
        }
        return arr
    end

    def attributeDictionariesJson(attribute_dictionaries = nil)
        return [] if attribute_dictionaries == nil

        res = {}
        attribute_dictionaries.map {|attrdict|
            elem = {}
            attrdict.each_pair { | key, value | elem[key] = value }
            res[attribute_dictionary.name] = elem
        }
        return res
    end
    def attributeDictionaryJson(attribute_dictionary = nil)
        return {} if attribute_dictionary == nil

        res = {}
        attribute_dictionary.each_pair {| key, value | res[key] = value }
        return res
    end

    def entityJson(entity = nil, tr = Geom::Transformation.new, fullentity = false)
        return nil if entity == nil

        res = {}
        entityClass = ''
        case entity
            when Sketchup::Layer
                entityClass = 'Layer'
                res['visible'] = entity.visible?
                res['name'] = entity.name
                res['color'] = entity.color
            when Sketchup::Group
                entityClass = 'Group'
                res['visible'] = entity.visible?
            when Sketchup::ComponentInstance
                entityClass = 'ComponentInstance'
                res['volume'] = entity.volume.to_m.to_m.to_m
                res['visible'] = entity.visible?
                res['layer'] = entityJson entity.layer
                definition = entityJson entity.definition
                res['definition'] = definition if definition
            when Sketchup::ComponentDefinition
                entityClass = 'ComponentDefinition'
                res['name'] = entity.name
                if fullentity
                    res['entities'] = entity.entities.map { |entity| entity.entityID }
                else
                    res['entities'] = entitiesJson entity.entities, tr, fullentity
                end
            when Sketchup::Face
                entityClass = 'Face'
                res['area'] = entity.area.to_m.to_m
                res['normal'] = vector3dJson entity.normal
            when Sketchup::Edge
                entityClass = 'Edge'
                res['length'] = entity.length.to_m
            when Sketchup::AttributeDictionary
                throw 'AttributeDictionary'
            else
                # puts "unknown class for #{entity}"
                return nil
        end
        res['entityID'] = entity.entityID
        res['persistentID'] = entity.persistent_id
        res['class'] = entityClass

        res['attributes'] = attributeDictionaryJson(entity.attribute_dictionary 'homebuilder')
        # begin
        #     res['attribute_dictionaries'] = attributeDictionariesJson entity.attribute_dictionaries()
        # rescue
        #     puts "attribute_dictionaries isn't exist #{entity}"
        #     # return nil
        # end

        return res
    end
