
    require 'sketchup.rb'

    def check_attribute(entity, key, default_value)
        val = entity.get_attribute 'dynamic_attributes', key
        if val == nil
            entity.set_attribute 'dynamic_attributes', key, default_value
        end
    end
    def set_attribute(entity, key, value)
        val = entity.get_attribute 'dynamic_attributes', key
        if val != value
            v = entity.set_attribute 'dynamic_attributes', key, value
            # puts "#{key} - #{value}     #{v}"
        end
    end
    def get_attribute(entity, key)
        return entity.get_attribute 'dynamic_attributes', key
    end

    def check_field(entity, name, label, units, default, access, options = nil)
        set_attribute entity, "_#{name}_label", name
        set_attribute entity, "_#{name}_formlabel", label
        set_attribute entity, "_#{name}_units", units
        set_attribute entity, "_#{name}_access", access
        if options
            set_attribute entity, "_#{name}_options", options.map {|id, option| "#{option.label}=#{id}"}.join('&')
            value = get_attribute entity, "#{name}"
            set_attribute entity, "#{name}", options.keys[0] if !options.key?(value)
        else
            check_attribute entity, "#{name}", default
        end
    end

    def remove_field(entity, name)
        # puts "remove_field #{name}"
        entity.delete_attribute 'dynamic_attributes', "#{name}"
        entity.delete_attribute 'dynamic_attributes', "_#{name}_label"
        entity.delete_attribute 'dynamic_attributes', "_#{name}_formlabel"
        entity.delete_attribute 'dynamic_attributes', "_#{name}_units"
        entity.delete_attribute 'dynamic_attributes', "_#{name}_access"
        entity.delete_attribute 'dynamic_attributes', "_#{name}_options"
    end

    def clear_field(entity, required_fields, all_fields = $AllFields)
        all_fields.each {|field|
            remove_field entity, field if !required_fields.include? field
        }
    end

    def reloadPlugin
        # load 'homebuilder/src/main.rb'
        load 'homebuilder/src/tools.rb'

        # load 'homebuilder/src/dialog.rb'
        load 'homebuilder/src/model.rb'
        load 'homebuilder/src/entity.rb'
        load 'homebuilder/src/tools/wall.rb'
        load 'homebuilder/src/tools/roof.rb'
        load 'homebuilder/src/tools/ray.rb'
    end
