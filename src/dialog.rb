
    require 'sketchup.rb'
    require 'json'
    require 'base64'

    $dialog = nil
    $reloadEnable = true

    def reloadAll
        if $reloadEnable
            puts "reloadAll"
            call_js 'updateModel', modelJson(Sketchup.active_model)
        end
    end

    def dialog_init
        $dialog.close if $dialog
        $dialog = UI::HtmlDialog.new({
            :dialog_title => 'Home Builder',
            :preferences_key => 'com.hb.plugin',
            :scrollable => true,
            :resizable => true,
            # :width => 600,
            # :height => 400,
            # :left => 100,
            # :top => 100,
            :min_width => 50,
            :min_height => 50,
            :max_width =>1000,
            :max_height => 1000,
            :style => UI::HtmlDialog::STYLE_DIALOG
        })
        # $dialog.set_can_close { false }
        path = Sketchup.find_support_file "homebuilder/pub/index.html", "Plugins"
        $dialog.set_file path
        $dialog.add_action_callback('reloadAll') { |action_context|
            reloadAll
        }
        $dialog.add_action_callback('getView') { |action_context|
            view = Sketchup.active_model.active_view
            status = view.write_image "test.jpg"
            data = Base64.encode64(File.open("test.jpg", "rb").read).gsub("\n", '')

            $dialog.execute_script "loadImage('data:image/png;base64," + data + "')"
        }
        $dialog.add_action_callback('setAttribute') { |action_context, entityId, key, value|
            model = Sketchup.active_model
            entity = model.find_entity_by_id entityId
            if entity
                $reloadEnable = false
                model.start_operation("Set HomeBuilder props", true)
                entity.set_attribute 'homebuilder', key, value
                model.commit_operation
                $reloadEnable = true
                reloadAll
            end
        }
        $dialog.add_action_callback('setName') { |action_context, entityId, name|
            model = Sketchup.active_model
            definition = model.find_entity_by_id entityId
            if definition
                $reloadEnable = false
                model.start_operation("Set HomeBuilder props", true)
                definition.name = name
                model.commit_operation
                $reloadEnable = true
                reloadAll
            end
        }
        $dialog.add_action_callback('replaceAttributes') { |action_context, entityId, attributes|
            model = Sketchup.active_model
            entity = model.find_entity_by_id entityId
            if entity
                $reloadEnable = false
                model.start_operation("Set HomeBuilder props", true)
                entity.delete_attribute 'homebuilder'
                if attributes
                    attributes.each_pair{|key, value|
                        entity.set_attribute 'homebuilder', key, value
                    }
                end
                model.commit_operation
                $reloadEnable = true
                reloadAll
            end
        }
        $dialog.show
    end

    def dialog_show
        dialog_init
    end

    def call_js(method, a0 = nil, a1 = nil, a2 = nil, a3 = nil, a4 = nil, a5 = nil, a6 = nil, a7 = nil, a8 = nil, a9 = nil)
        return if !$dialog
        arr = []
        arr.push JSON.generate(a0) rescue arr.push(a0.to_json) if a0 != nil
        arr.push JSON.generate(a1) rescue arr.push(a1.to_json) if a1 != nil
        arr.push JSON.generate(a2) rescue arr.push(a2.to_json) if a2 != nil
        arr.push JSON.generate(a3) rescue arr.push(a3.to_json) if a3 != nil
        arr.push JSON.generate(a4) rescue arr.push(a4.to_json) if a4 != nil
        arr.push JSON.generate(a5) rescue arr.push(a5.to_json) if a5 != nil
        arr.push JSON.generate(a6) rescue arr.push(a6.to_json) if a6 != nil
        arr.push JSON.generate(a7) rescue arr.push(a7.to_json) if a7 != nil
        arr.push JSON.generate(a8) rescue arr.push(a8.to_json) if a8 != nil
        arr.push JSON.generate(a9) rescue arr.push(a9.to_json) if a9 != nil

        arr_str = arr.join '\',\''
        $dialog.execute_script "#{method}('#{arr_str}')"
    end
