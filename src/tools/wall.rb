
    require 'sketchup.rb'
    require 'json'
    require 'base64'

    def tools_wall
        dialog = UI::HtmlDialog.new({
            :dialog_title => 'Create the wall',
            :preferences_key => 'com.hb.plugin',
            :scrollable => true,
            :resizable => true,
            # :width => 600,
            # :height => 400,
            # :left => 100,
            # :top => 100,
            :min_width => 50,
            :min_height => 50,
            :max_width =>1000,
            :max_height => 1000,
            :style => UI::HtmlDialog::STYLE_DIALOG
        })
        path = Sketchup.find_support_file "homebuilder/pub/wall.html", "Plugins"
        dialog.set_file path
        dialog.show
    end

    def tools_wall_init
        cmd = UI::Command.new("Wall tool") { tools_wall }
        # cmd.small_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        # cmd.large_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        cmd.small_icon = "ToolPencilSmall.png"
        cmd.large_icon = "ToolPencilLarge.png"
        cmd.tooltip = "Wall tool"
        cmd.status_bar_text = "Create the wall"
        cmd.menu_text = "Wall tool"
        return cmd
    end
