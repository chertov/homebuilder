
    require 'sketchup.rb'
    require 'json'
    require 'base64'

    def ray_tool
    #     dialog = UI::HtmlDialog.new({
    #         :dialog_title => 'Create the wall',
    #         :preferences_key => 'com.hb.plugin',
    #         :scrollable => true,
    #         :resizable => true,
    #         # :width => 600,
    #         # :height => 400,
    #         # :left => 100,
    #         # :top => 100,
    #         :min_width => 50,
    #         :min_height => 50,
    #         :max_width =>1000,
    #         :max_height => 1000,
    #         :style => UI::HtmlDialog::STYLE_DIALOG
    #     })
    #     path = Sketchup.find_support_file "homebuilder/pub/wall.html", "Plugins"
    #     dialog.set_file path
    #     dialog.show


        load 'homebuilder/src/tools/ray.rb'
        puts '---------------------'
        model = Sketchup.active_model
        selection = model.selection
        entities = model.entities
        return if selection.length != 1
        edge = selection[0]
        return if !edge.is_a?(Sketchup::Edge)

        model = Sketchup.active_model
        ray = [edge.start.position, edge.end.position]
        ray = model.raytest(ray, true) # Consider hidden geometry when computing intersections.

        depths = ray[0]
        items = ray[1]
        items.map {|item|
            case item
                # when Sketchup::Layer
                # when Sketchup::Group
                when Sketchup::ComponentInstance
                    puts "    item #{item.definition.name}"
                # when Sketchup::ComponentDefinition
                # when Sketchup::Face
                #     puts "    item #{item.definition.name}"
                # when Sketchup::Edge
                # when Sketchup::AttributeDictionary
                else
                    puts "    item #{item}"
            end
        }
    end

    def ray_tool_init
        cmd = UI::Command.new("Ray tool") { ray_tool }
        # cmd.small_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        # cmd.large_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        cmd.small_icon = "ToolPencilSmall.png"
        cmd.large_icon = "ToolPencilLarge.png"
        cmd.tooltip = "Ray tool"
        cmd.status_bar_text = "Create the Ray"
        cmd.menu_text = "Ray tool"
        return cmd
    end
