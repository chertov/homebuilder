
    class MyTool
        def activate
            puts 'Roof tool has been activated.'
            model = Sketchup.active_model
            selection = model.selection

            selection.each {|entity|
                puts "selection #{entity}"
            }
        end
        def deactivate(view)
            puts "Roof tool has been deactivated in view: #{view}"
        end

        def draw(view)
            # Draw a square.
            points = [
                Geom::Point3d.new(0, 0, 0),
                Geom::Point3d.new(9, 0, 0),
                Geom::Point3d.new(9, 9, 0),
                Geom::Point3d.new(0, 9, 0)
            ]
            # Fill
            view.drawing_color = Sketchup::Color.new(255, 128, 128)
            view.draw(GL_QUADS, points)
            # Outline
            view.line_stipple = '' # Solid line
            view.drawing_color = Sketchup::Color.new(64, 0, 0)
            view.draw(GL_LINE_LOOP, points)
            view.refresh
            view.invalidate
        end

        def getExtents
            bb = Sketchup.active_model.bounds
            p = bb.center
            bb.add([p.x - 1000, p.y - 1000, p.z - 1000], [p.x + 1000, p.y + 1000, p.z + 1000])
            return bb
        end
    end

    # $my_tool = MyTool.new
    def nextEdgePoint(edges, index)
        nextIndex = index + 1
        nextIndex = 0 if index == edges.length-1

        e1s = edges[index].start.position
        e1e = edges[index].end.position
        e2s = edges[nextIndex].start.position
        e2e = edges[nextIndex].end.position
        return [e1s, e1e, e2e] if  e1e == e2s # e1s -- e1e e2s -- e2e
        return [e1s, e1e, e2s] if  e1e == e2e # e1s -- e1e e2e -- e2s
        return [e1e, e1s, e2e] if  e1s == e2s # e1e -- e1s e2s -- e2e
        return [e1e, e1s, e2s] if  e1s == e2e # e1e -- e1s e2e -- e2s

        # if index == edges.length-1
        #     return [edges[index].start.position, edges[index].end.position, edges[0].end.position]
        # end
        # return [edges[index].start.position, edges[index].end.position, edges[index+1].end.position]
    end

    def roof_tool
        load 'homebuilder/src/tools/roof.rb'
        puts '---------------------'
        model = Sketchup.active_model
        selection = model.selection
        entities = model.entities
        return if selection.length != 1
        face = selection[0]
        return if !face.is_a?(Sketchup::Face)
        edges = face.outer_loop.edges
        return if edges.length < 3

        angle = 30.degrees
        model.start_operation("Create roof", true)

        last = face.outer_loop.edges.length - 1
        face.outer_loop.edges.each_with_index {|edge, index|
            puts "#{index} ============= "
            points = nextEdgePoint edges,index
            p1_pos = points[0]
            p2_pos = points[1]
            p3_pos = points[2]

            p1 = Geom::Vector3d.new p1_pos.x, p1_pos.y, p1_pos.z
            p2 = Geom::Vector3d.new p2_pos.x, p2_pos.y, p2_pos.z
            p3 = Geom::Vector3d.new p3_pos.x, p3_pos.y, p3_pos.z

            l1 = (p1-p2).length
            l2 = (p3-p2).length
            full_len = l1 + l2
            p_sr = Geom::Vector3d.linear_combination(l2/full_len, p1, l1/full_len, p3)
            p_dir = (p_sr - p2).normalize
            p_dir.length = 1
            result = face.classify_point Geom::Point3d.new((p_dir + p2).x,(p_dir + p2).y,(p_dir + p2).z)
            if result == Sketchup::Face::PointOutside
                p_dir.reverse!
            end

            p_dir.z += Math.sin(angle)
            p_dir.length = 10000
            p_sr = p_dir + p2
            p_sr = Geom::Point3d.new(p_sr.x,p_sr.y,p_sr.z)
            entities.add_edges p2_pos, p_sr # if index == last

            # pts[0] = [0, 0, 0]
            # pts[1] = [width, 0, 0]
            # pts[2] = [width, depth, 0]
            # pts[3] = [0, depth, 0]
            # # Add the face to the entities in the model
            # face = entities.add_face pts

            # puts "#{prevEdge.start.position.x} #{prevEdge.start.position.y} #{prevEdge.start.position.z}"
            # puts "#{edge.start.position.x} #{edge.start.position.y} #{edge.start.position.z}"
            # puts "#{edge.end.position.x} #{edge.end.position.y} #{edge.end.position.z}"
            # puts "#{nextEdge.end.position.x} #{nextEdge.end.position.y} #{nextEdge.end.position.z}"
            # puts '---'
            # puts "#{prevEdge.start.position.x} #{prevEdge.start.position.y} #{prevEdge.start.position.z}"
            # puts "#{prevEdge.end.position.x} #{prevEdge.end.position.y} #{prevEdge.end.position.z}"
            # puts "#{edge.start.position.x} #{edge.start.position.y} #{edge.start.position.z}"
            # puts "#{edge.end.position.x} #{edge.end.position.y} #{edge.end.position.z}"
            # puts "#{nextEdge.start.position.x} #{nextEdge.start.position.y} #{nextEdge.start.position.z}"
            # puts "#{nextEdge.end.position.x} #{nextEdge.end.position.y} #{nextEdge.end.position.z}"
        }

        model.commit_operation
        # Sketchup.active_model.select_tool($my_tool)
    end


    def roof_tool_init
        cmd = UI::Command.new("Roof tool") { roof_tool }
        # cmd.small_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        # cmd.large_icon = Sketchup.find_support_file "homebuilder/pub/images/toolwall2.png", "Plugins"
        cmd.small_icon = "ToolPencilSmall.png"
        cmd.large_icon = "ToolPencilLarge.png"
        cmd.tooltip = "Roof tool"
        cmd.status_bar_text = "Create the roof"
        cmd.menu_text = "Roof tool"
        return cmd
    end
