
    require 'sketchup.rb'
    require_relative 'entity.rb'
    require_relative 'model.rb'

    class MyModelObserver < Sketchup::ModelObserver
        def onActivePathChanged(model)
            # puts "onActivePathChanged: #{model}"
            call_js 'onActivePathChanged', modelJson(model)
        end
        def onPlaceComponent(instance)
            # puts "onPlaceComponent: #{instance}"
            call_js 'onPlaceComponent', entityJson(instance)
        end
    end

    class MyEntitiesObserver < Sketchup::EntitiesObserver
        def onActiveSectionPlaneChanged(entities)
            entitiesjs = entitiesJson(entities)
            call_js 'onActiveSectionPlaneChanged', entitiesjs if entitiesjs
        end
        def onElementAdded(entities, entity)
            reloadAll
            # entitiesjs = entitiesJson(entities)
            # entityjs = entityJson(entity)
            # call_js 'onElementAdded', entitiesjs, entityjs if entitiesjs && entityjs
        end
        def onElementModified(entities, entity)
            reloadAll
            # entitiesjs = entitiesJson(entities)
            # entityjs = entityJson(entity)
            # call_js 'onElementModified', entitiesjs, entityjs if entitiesjs && entityjs
        end
        def onElementRemoved(entities, entity_id)
            reloadAll
            # entitiesjs = entitiesJson(entities)
            # call_js 'onElementRemoved', entitiesjs, entity_id if entitiesjs
        end
        def onEraseEntities(entities)
            reloadAll
            # entitiesjs = entitiesJson(entities)
            # call_js 'onEraseEntities', entitiesjs if entitiesjs
        end
    end

    class MySelectionObserver < Sketchup::SelectionObserver
        def onSelectionAdded(selection, entity)
            call_js 'onSelectionAdded', entitiesJson(selection), entityJson(entity)
        end
        def onSelectionBulkChange(selection)
            call_js 'onSelectionBulkChange', entitiesJson(selection)
        end
        def onSelectionCleared(selection)
            call_js 'onSelectionCleared', entitiesJson(selection)
        end
        def onSelectionRemoved(selection, entity)
            call_js 'onSelectionRemoved', entitiesJson(selection), entityJson(entity)
        end
    end

    class MyInstanceObserver < Sketchup::InstanceObserver
        def onOpen(instance)
            # call_js 'onInstanceOpen', entityJson(instance)
        end
        def onClose(instance)
            # call_js 'onInstanceClose', entityJson(instance)
        end
    end

    class MyDefinitionsObserver < Sketchup::DefinitionsObserver
        def onComponentAdded(definitions, definition)
            # puts "onComponentAdded: #{definition.name}"
        end
        def onComponentPropertiesChanged(definitions, definition)
            # puts "onComponentPropertiesChanged: #{definition}"
        end
    end

    class MyAppObserver < Sketchup::AppObserver
        # def onActivateModel(model)
        #     self.setObservers(model)
        # end
        def onNewModel(model)
            self.setObservers(model)
        end
        def onOpenModel(model)
            self.setObservers(model)
        end
        def onQuit
        end
        def onUnloadExtension(extension_name)
        end

        def setObservers(model)
            model.definitions.add_observer(MyDefinitionsObserver.new)
            model.entities.add_observer(MyEntitiesObserver.new)
            model.selection.add_observer(MySelectionObserver.new)
            model.add_observer(MyModelObserver.new)
            reloadAll
        end
    end

    appobserver = MyAppObserver.new
    appobserver.setObservers Sketchup.active_model
    Sketchup.add_observer(appobserver)
