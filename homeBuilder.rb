
    require "sketchup.rb"
    require "extensions.rb"

    # Load plugin as extension (so that user can disable it)

    homeBuilder = SketchupExtension.new "Home Builder", "homebuilder/src/main.rb"
    homeBuilder.copyright= "Copyright 2011 by Me"
    homeBuilder.creator= "Me, myself and I"
    homeBuilder.version = "1.0"
    homeBuilder.description = "Description of plugin."
    Sketchup.register_extension homeBuilder, true
