
import React from 'react';
import ReactDOM from 'react-dom/server';
import { Provider } from 'react-redux';
import { RouterContext } from 'react-router';

import getStore from './../jsx/store';
import { getMeta } from './../jsx/tools';

const renderHtml = (initStateValue, renderProps, req, res, client) => {

    const store = getStore(Object.assign({}, initStateValue, { tree: getTree() }));

    res.checkHeaders = () => res.headersSent;

    const htmlTemplate = <html lang='ru'>

    let htmlStr = `<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlTemplate)}`;

    if (res !== null && !res.headersSent) {
        const meta = getMeta();

        htmlStr = htmlStr.replace('metaTitle', meta.title);
        res.status(200);
        res.send(htmlStr);
        res.end();
        return true;
    }
    return htmlTemplate;
};

export default renderHtml;
