
import { SET_MODEL } from 'reducers/model';
import getStore from 'store';
import { base64ToObj } from 'tools';

import Wire from 'ui/properties/wire.jsx';
import Volume from 'ui/properties/volume.jsx';
import Plane from 'ui/properties/plane.jsx';
import FixPrice from 'ui/properties/fixprice.jsx';
import Plate from 'ui/properties/plate.jsx';

export const setModel = model => {
    let price = 0;

    model.entities.forEach(entity => {
        let objtype = null;

        try { if (entity.attributes.objtype) { objtype = entity.attributes.objtype; } } catch (e) { }
        try { if (entity.attributes.addExpenses) { entity.attributes.addExpenses = base64ToObj(entity.attributes.addExpenses); } } catch (e) { }

        entity.weight = 0;
        entity.price = 0;
        if (!entity.volume) entity.volume = 0;
        if (!entity.area) entity.area = 0;
        if (!entity.length) entity.length = 0;
        if (entity.attributes.addExpenses) {
            entity.price = entity.attributes.addExpenses.reduce((sum, expense) => sum + expense.price, 0);
        }
        switch (objtype) {
            case 'wire': {
                entity = Wire.calc(entity);
                if (entity.visible && entity.layer.visible) { price += entity.price; }
                break;
            }
            case 'plane': {
                entity = Plane.calc(entity);
                if (entity.visible && entity.layer.visible) { price += entity.price; }
                break;
            }
            case 'volume': {
                entity = Volume.calc(entity);
                if (entity.visible && entity.layer.visible) { price += entity.price; }
                break;
            }
            case 'fixprice': {
                entity = FixPrice.calc(entity);
                if (entity.visible && entity.layer.visible) { price += entity.price; }
                break;
            }
            case 'plate': {
                entity = Plate.calc(entity);
                if (entity.visible && entity.layer.visible) { price += entity.price; }
                break;
            }
            default:
        }
    });
    model.price = price;

    console.log('model', model);

    getStore().dispatch({ type: SET_MODEL, model });
};
