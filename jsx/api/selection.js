
import { SET_SELECTION, SET_ACTIVEPATH } from 'reducers/selection';
import getStore from 'store';

export const setSelection = selection => {
    getStore().dispatch({ type: SET_SELECTION, selection });
};
export const setActivePath = activePath => {
    getStore().dispatch({ type: SET_ACTIVEPATH, activePath });
};
