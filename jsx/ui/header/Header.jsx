
import React, { Component, PropTypes as pt } from 'react';
import { Link } from 'react-router';
import pdfExport from 'pdf';

import './ui-header.less';

class Header extends Component {
    static propTypes = {
        reloadLink: pt.string
    }

    render() {
        const { reloadLink } = this.props;

        return <ul className='header'>
            <li><div onClick={() => location.href = reloadLink} >Reload</div></li>
            <li><Link to='/materials'>Materials</Link></li>
            <li><Link to='/'>Edit tree</Link></li>
            <li><div onClick={() => pdfExport()} >PDF export</div></li>
        </ul>;
    }
}

export default Header;
