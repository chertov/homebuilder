
import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';

import { formatPrice } from 'tools';
import { notset, NotSetOption, ObjectTypes } from 'materials';
import Wire from './wire.jsx';
import Plane from './plane.jsx';
import Volume from './volume.jsx';
import FixPrice from './fixprice.jsx';
import Plate from './plate.jsx';
import AddExpenses from './addexpenses.jsx';

import './ui-properties.less';

class Properties extends Component {
    static propTypes = {
        selection: pt.object,
        model: pt.object
    }
    state = { }

    componentWillReceiveProps(nextProps) {
        this.setState({ definitionID: null, name: null });
    }

    handleNameChange(definitionID, name) {
        clearTimeout(this.timeoutUpdate);
        this.timeoutUpdate = setTimeout(this.setName, 1000);
        this.setState({ definitionID, name });
    }
    handleTypeChange(entity = null, type = null) {
        if (entity) {
            if (type === notset) {
                sketchup.replaceAttributes(entity.entityID);
            } else {
                sketchup.replaceAttributes(entity.entityID, { objtype: type });
            }
        }
    }
    setName = () => {
        const { definitionID, name } = this.state;

        if (definitionID && name) {
            sketchup.setName(definitionID, name);
        }
    }
    timeoutUpdate = null;

    calcSelected() {
        const { model } = this.props;
        const { selection } = this.props.selection;
        let res = { weight: 0, price: 0, volume: 0, area: 0, length: 0 };

        if (selection.length > 0) {
            selection.forEach(entityID => {
                if (!model) { return; }
                const entity = model.entities.find(entity => entity.entityID === entityID);

                if (entity) {
                    res.weight += entity.weight;
                    res.price += entity.price;
                    res.volume += entity.volume;
                    res.area += entity.area;
                    res.length += entity.length;
                }
            });
        }

        return res;
    }

    render() {
        const { model } = this.props;
        const state = this.props.selection;

        let entity = null;

        if (state.activePath.length > 0) {
            state.activePath.forEach(entityID => {
                if (!model) { return; }
                const activeEntity = model.entities.find(entity => entity.entityID === entityID);

                if (activeEntity && activeEntity.class === 'ComponentInstance') { entity = activeEntity; }
            });
        }
        if (entity === null && state.selection.length > 0) {
            state.selection.forEach(entityID => {
                if (!model) { return; }
                const activeEntity = model.entities.find(entity => entity.entityID === entityID);

                if (activeEntity && activeEntity.class === 'ComponentInstance') { entity = activeEntity; }
            });
        }

        let objtype = notset;
        let elem = '';

        try { if (entity.attributes.objtype) { objtype = entity.attributes.objtype; } } catch (e) { }

        switch (objtype) {
            case 'wire': {
                elem = <Wire entity={entity} />;
                break;
            }
            case 'plane': {
                elem = <Plane entity={entity} />;
                break;
            }
            case 'volume': {
                elem = <Volume entity={entity} />;
                break;
            }
            case 'fixprice': {
                elem = <FixPrice entity={entity} />;
                break;
            }
            case 'plate': {
                elem = <Plate entity={entity} />;
                break;
            }
            default:
        }

        // Selection:
        // {JSON.stringify(state.selection, null, 4)}
        // <br />
        // Active Path:
        // {JSON.stringify(state.activePath, null, 4)}
        // Properties for:
        // {JSON.stringify(entity, null, 4)}

        let definitionID = entity ? entity.definition.entityID : null;
        let name = entity ? entity.definition.name : null;
        let fullhouseprice = '';

        definitionID = this.state.definitionID ? this.state.definitionID : definitionID;
        name = this.state.name ? this.state.name : name;
        if (model) { fullhouseprice = <div>Full house price: {formatPrice(model.price)}</div>; }

        const selectedInfo = this.calcSelected();

        return (<div>
            {
                entity ?
                (
                    <div>
                        <br />
                        Name: <input
                            type='text'
                            onChange={event => this.handleNameChange(definitionID, event.target.value)}
                            value={name}
                        />
                        <br />
                        Type object:
                        <select onChange={event => this.handleTypeChange(entity, event.target.value)} value={objtype}>
                            <option key={notset} value={notset}>Not set</option>
                            { Object.keys(ObjectTypes).map(objtypeid => <option key={objtypeid} value={objtypeid}>{ObjectTypes[objtypeid]}</option>) }
                        </select>
                        {elem}
                        { objtype !== notset ? <AddExpenses entity={entity} /> : '' }
                    </div>
                ) : ''
            }
            {
                // state.selection.length > 1 ? <br>Selected:</br> : ''
            }
            <br /> Price: {formatPrice(selectedInfo.price)}
            <br /> Volume: {selectedInfo.volume.toFixed(2)} m³
            <br /> Area: {selectedInfo.area.toFixed(2)} m²
            <br /> Length: {selectedInfo.length.toFixed(1)} m
            <br /> Weight selected: {selectedInfo.weight.toFixed(2)} kg
            <br />
            <br /> {fullhouseprice}
        </div>);
    }
}

// мапим сосотяние стора на props компонента
const mapStateToProps = state => ({ selection: state.selection, model: state.model });

// экспортируем компонент с замаппеным сосотянием и экшенами
export default connect(mapStateToProps)(Properties);
