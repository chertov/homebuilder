
import React, { Component, PropTypes as pt } from 'react';
import { getMaterial, notset, NotSetOption, planematerials } from 'materials';
import { calcArea, formatPrice } from 'tools';

class Plane extends Component {
    static propTypes = {
        entity: pt.object
    }
    static calc(entity) {
        entity.fullarea = calcArea(entity);
        entity.material = getMaterial(entity);
        entity.area = entity.fullarea;
        entity.price += entity.material.price * entity.fullarea;
        entity.weight = entity.material.weight * entity.fullarea;
        return entity;
    }

    handleMaterialChange(materialId) {
        const { entity } = this.props;

        if (entity) {
            sketchup.setAttribute(entity.entityID, 'materialId', materialId);
        }
    }
    render() {
        const { entity } = this.props;

        return (<div>
            Material:
            <select onChange={event => this.handleMaterialChange(event.target.value)} value={entity.material.id}>
                <option key={notset} value={notset}>Not set</option>
                { Object.keys(planematerials).map(typeid => <option key={typeid} value={typeid}>{planematerials[typeid].label}</option>) }
            </select>
        </div>);
    }
}

export default Plane;
