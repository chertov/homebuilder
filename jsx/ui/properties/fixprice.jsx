
import React, { Component, PropTypes as pt } from 'react';
import { formatPrice } from 'tools';

class FixPrice extends Component {
    static propTypes = {
        entity: pt.object
    }
    static calc(entity) {
        entity.fixprice = 0;
        try { if (entity.attributes.fixprice) { entity.fixprice = entity.attributes.fixprice; } } catch (e) { }
        entity.price += entity.fixprice;
        return entity;
    }

    handleFixPriceChange(price) {
        const { entity } = this.props;

        if (entity) {
            sketchup.setAttribute(entity.entityID, 'fixprice', price);
        }
    }
    render() {
        const { entity } = this.props;

        return <div>
            Fix price: <input
                type='number'
                min='1' max='999999999999999'
                onChange={event => this.handleFixPriceChange(event.target.value)}
                value={entity.fixprice}
            />
            <br />
            Price: {formatPrice(entity.price)}
        </div>;
    }
}

export default FixPrice;
