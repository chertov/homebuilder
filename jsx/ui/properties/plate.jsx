
import React, { Component, PropTypes as pt } from 'react';
import { defaultMaterial, getMaterialById, notset, NotSetOption, concretes, armatures } from 'materials';
import { formatPrice } from 'tools';

export const getMaxArea = entity => {
    let entities = [];

    try { if (entity.definition.entities) { entities = entity.definition.entities; } } catch (e) { return 0; }
    const areas = entities.filter(entity => entity && entity.class === 'Face').map(face => face.area);

    return Math.max.apply(null, areas);
};

class Plate extends Component {
    static propTypes = {
        entity: pt.object
    }
    static calc(entity) {
        entity.fullarea = getMaxArea(entity);
        entity.thickness = entity.volume / entity.fullarea;
        entity.area = entity.fullarea;

        entity.concrete = defaultMaterial;
        try { if (entity.attributes.concreteId) { entity.concrete = getMaterialById(entity.attributes.concreteId); } } catch (e) { }

        entity.armature = defaultMaterial; entity.armatureLayouts = 2; entity.armatureStep = 20;
        try { if (entity.attributes.armatureId) { entity.armature = getMaterialById(entity.attributes.armatureId); } } catch (e) { }
        try { if (entity.attributes.armatureLayouts) { entity.armatureLayouts = entity.attributes.armatureLayouts; } } catch (e) { }
        try { if (entity.attributes.armatureStep) { entity.armatureStep = entity.attributes.armatureStep; } } catch (e) { }
        entity.armatureLen = entity.fullarea * 2.0 / (entity.armatureStep * 0.01) * entity.armatureLayouts;

        entity.concretePrice = entity.concrete.price * entity.volume;
        entity.armaturePrice = entity.armature.price * entity.armatureLen;
        entity.concreteWeight = entity.concrete.weight * entity.volume;
        entity.armatureWeight = entity.armature.weight * entity.armatureLen;
        entity.price += entity.concretePrice + entity.armaturePrice;
        entity.weight = entity.concreteWeight + entity.armatureWeight;
        return entity;
    }

    handleConcreteChange(entity, materialId) {
        sketchup.setAttribute(entity.entityID, 'concreteId', materialId);
    }
    handleArmtureChange(entity, materialId) {
        sketchup.setAttribute(entity.entityID, 'armatureId', materialId);
    }
    handleLayoutsCountChange(entity, value) {
        sketchup.setAttribute(entity.entityID, 'armatureLayouts', value);
    }
    handleStepChange(entity, value) {
        sketchup.setAttribute(entity.entityID, 'armatureStep', value);
    }
    render() {
        const { entity } = this.props;

        return (<div>
            Concrete:
            <select onChange={event => this.handleConcreteChange(entity, event.target.value)} value={entity.concrete.id}>
                <option key={notset} value={notset}>Not set</option>
                { Object.keys(concretes).map(typeid => <option key={typeid} value={typeid}>{concretes[typeid].label}</option>) }
            </select>
            <br />
            Armature:
            <select onChange={event => this.handleArmtureChange(entity, event.target.value)} value={entity.armature.id}>
                <option key={notset} value={notset}>Not set</option>
                { Object.keys(armatures).map(typeid => <option key={typeid} value={typeid}>{armatures[typeid].label}</option>) }
            </select>
            <br />
            Armature layouts:<input
                type='number'
                min='1' max='100'
                onChange={event => this.handleLayoutsCountChange(entity, event.target.value)}
                value={entity.armatureLayouts}
            />
            <br />
            Armature step:<input
                type='number'
                min='5' max='500'
                onChange={event => this.handleStepChange(entity, event.target.value)}
                value={entity.armatureStep}
            /> cm
            <br />
            ====================================
            <br />
            Area: {entity.fullarea.toFixed(2)} m²
            <br />
            Thickness: {(entity.thickness * 100).toFixed(1)} cm
            <br />
            Concrete volume: {entity.volume.toFixed(2)} m³
            <br />
            Armature length: {entity.armatureLen.toFixed(2)} m
            <br />
            Concrete weight: {entity.concreteWeight.toFixed(2)} kg
            <br />
            Armature weight: {entity.armatureWeight.toFixed(2)} kg
            <br />
            Concrete price: {formatPrice(entity.concretePrice)}
            <br />
            Armature price: {formatPrice(entity.armaturePrice)}
        </div>);
    }
}

export default Plate;
