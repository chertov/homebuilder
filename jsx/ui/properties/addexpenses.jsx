
import React, { Component, PropTypes as pt } from 'react';
import { formatPrice, objToBase64 } from 'tools';

class AddExpenses extends Component {
    static propTypes = {
        entity: pt.object,
        addExpenses: pt.array
    }
    state = {}
    componentWillReceiveProps(nextProps) {
        this.setState({ addExpenses: null });
    }

    setAddExpenses = () => {
        const { entity } = this.props;
        const { addExpenses } = this.state;

        if (addExpenses) {
            sketchup.setAttribute(entity.entityID, 'addExpenses', objToBase64(addExpenses));
        }
    }
    timeoutUpdate = null;
    update(addExpenses) {
        clearTimeout(this.timeoutUpdate);
        this.timeoutUpdate = setTimeout(this.setAddExpenses, 1000);
        this.setState({ addExpenses });
    }

    handleAdd(addExpenses) {
        addExpenses.push({ name: 'work', price: 1.0 });
        this.update(addExpenses);
    }
    handleRemove(addExpenses, idx) {
        if (idx >= 0 && idx < addExpenses.length) {
            addExpenses.splice(idx, 1);
            this.update(addExpenses);
        }
    }
    handleNameChange(addExpenses, idx, name) {
        if (idx >= 0 && idx < addExpenses.length) {
            addExpenses[idx].name = name;
            this.update(addExpenses);
        }
    }
    handlePriceChange(addExpenses, idx, price) {
        if (idx >= 0 && idx < addExpenses.length) {
            addExpenses[idx].price = parseFloat(price);
            this.update(addExpenses);
        }
    }

    render() {
        const { entity } = this.props;
        let addExpenses = this.state.addExpenses ? this.state.addExpenses : null;

        if (entity.attributes.addExpenses && addExpenses === null) {
            addExpenses = entity.attributes.addExpenses
        }

        addExpenses = addExpenses ? addExpenses : [];

        return <div>
            AddExpenses: <span onClick={() => this.handleAdd(addExpenses)} >Add</span>
            <br />{
                addExpenses.map((expense, idx) =>
                <div key={idx}>
                    <input
                        type='text'
                        onChange={event => this.handleNameChange(addExpenses, idx, event.target.value)}
                        value={expense.name}
                    />
                    <input
                        type='number'
                        min='0' max='100000000'
                        step='1'
                        onChange={event => this.handlePriceChange(addExpenses, idx, event.target.value)}
                        value={expense.price}
                    />
                    <span onClick={() => this.handleRemove(addExpenses, idx)} >-</span>
                </div>)
            }
        </div>;
    }
}

export default AddExpenses;
