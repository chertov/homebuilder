
import React, { Component, PropTypes as pt } from 'react';
import { getMaterial, notset, NotSetOption, volumematerials } from 'materials';
import { formatPrice } from 'tools';

class Volume extends Component {
    static propTypes = {
        entity: pt.object
    }
    static calc(entity) {
        entity.material = getMaterial(entity);
        entity.price += entity.material.price * entity.volume;
        entity.weight = entity.material.weight * entity.volume;
        return entity;
    }
    handleMaterialChange(materialId) {
        const { entity } = this.props;

        if (entity) {
            sketchup.setAttribute(entity.entityID, 'materialId', materialId);
        }
    }
    render() {
        const { entity } = this.props;

        return (<div>
            Material:
            <select onChange={event => this.handleMaterialChange(event.target.value)} value={entity.material.id}>
                <option key={notset} value={notset}>Not set</option>
                { Object.keys(volumematerials).map(typeid => <option key={typeid} value={typeid}>{volumematerials[typeid].label}</option>) }
            </select>
        </div>);
    }
}

export default Volume;
