
import React, { Component, PropTypes as pt } from 'react';
import { getMaterial, notset, NotSetOption, wirematerials } from 'materials';
import { calcLength, formatPrice } from 'tools';

class Wire extends Component {
    static propTypes = {
        entity: pt.object
    }
    static calc(entity) {
        entity.fulllength = calcLength(entity);
        entity.material = getMaterial(entity);
        entity.length = entity.fulllength;
        entity.price += entity.material.price * entity.fulllength;
        entity.weight = entity.material.weight * entity.fulllength;
        return entity;
    }

    handleMaterialChange(materialId) {
        const { entity } = this.props;

        if (entity) {
            sketchup.setAttribute(entity.entityID, 'materialId', materialId);
        }
    }
    render() {
        const { entity } = this.props;

        return (<div>
            Material:
            <select onChange={event => this.handleMaterialChange(event.target.value)} value={entity.material.id}>
                <option key={notset} value={notset}>Not set</option>
                { Object.keys(wirematerials).map(typeid => <option key={typeid} value={typeid}>{wirematerials[typeid].label}</option>) }
            </select>
        </div>);
    }
}

export default Wire;
