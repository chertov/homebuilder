import { Router, Route, IndexRoute, Link, Redirect, IndexLink, browserHistory } from 'react-router';
import React, { PropTypes } from 'react';
import Main from 'ui/main.jsx';
import Header from 'ui/header';
import Materials from 'pages/materials';

function handleUpdate() {
    const { hash } = window.location;
    const { action, state } = this.state.location;

    const pathname = window.location.pathname;
    const url = window.location.href;


    console.log('url', pathname, url);
    // if (state && state.noScroll) {
    //     return;
    // }
    //
    // if (hash === '' && action === 'PUSH') {
    //     window.scrollTo(0, 0);
    // }
    //
    // if (hash !== '') {
    //     setTimeout(() => {
    //         const id = hash.replace('#', '');
    //         const element = document.getElementById(id);
    //
    //         if (element) {
    //             element.scrollIntoView();
    //         }
    //     }, 0);
    // }
}

const reloadLink = location.href;

browserHistory.push('/');

const App = ({ children }) =>
    <div>
        <Header reloadLink={reloadLink} />
        <br />
        <br />
        {children}
    </div>;

const Page404 = () =>
    <div>401<br /></div>;

App.propTypes = { children: PropTypes.element.isRequired };

export default (<Router
    history={browserHistory}
    onUpdate={handleUpdate}
>
    <Route path='/' component={App}>
        <IndexRoute component={Main} />
        <Route path='materials' component={Materials} />
    </Route>
    <Route path='*' component={Page404} />
</Router>);
