
import getStore from 'store';
import { formatPrice } from 'tools';
import { notset, getMaterial } from 'materials'

const genDoc = () => {
    const store = getStore();
    const { model } = store.getState();
    const layers = {};

    model.layers.forEach(layer => {
        layers[layer.entityID] = layer;
        layers[layer.entityID].entities = [];
    });

    const iterateEntities = entities => {
        entities.forEach(entity => {
            if (entity.class !== 'ComponentInstance') { return; }
            layers[entity.layer.entityID].entities.push(entity);
            if (entity.definition.entities) {
                iterateEntities(entity.definition.entities);
            }
        });
    };

    iterateEntities(model.entities);

    const rows = [];
    let fullprice = 0;

    Object.keys(layers).forEach(layerId => {
        const layer = layers[layerId];

        if (!layer.visible) { return; }
        if (layer.entities.length < 1) { return; }
        let layerprice = 0;

        rows.push([{ text: layer.name, style: 'tableHeader', colSpan: 4, alignment: 'center' }, {}, {}, {}]);
        layer.entities.forEach(entity => {
            layerprice += entity.price;
            let objtype = notset;

            try { if (entity.attributes.objtype) { objtype = entity.attributes.objtype; } } catch (e) { return; }

            switch (objtype) {
                case 'wire': {
                    rows.push([entity.definition.name, entity.material.label, entity.fulllength.toFixed(2) + ' м', formatPrice(entity.price)]);
                    break;
                }
                case 'plane': {
                    rows.push([entity.definition.name, entity.material.label, entity.fullarea.toFixed(2) + ' м²', formatPrice(entity.price)]);
                    break;
                }
                case 'volume': {
                    rows.push([entity.definition.name, entity.material.label, entity.volume.toFixed(2) + ' м³', formatPrice(entity.price)]);
                    break;
                }
                case 'fixprice': {
                    rows.push([entity.definition.name, ' ', ' ', formatPrice(entity.price)]);
                    break;
                }
                case 'plate': {
                    rows.push([{ text: entity.definition.name +
                        `       площадь: ${entity.fullarea.toFixed(2)} м², толщина: ${(entity.thickness*100).toFixed(1)} см`, style: 'entity', colSpan: 4, alignment: 'left' }, {}, {}, {}]);
                    rows.push([`\u200B        - бетон: ${entity.concrete.label}`,
                        { text: 'объем: ' + entity.volume.toFixed(2) + ' м³', alignment: 'right' },
                        { text: formatPrice(entity.concretePrice), alignment: 'right' }, '']);
                    rows.push([`\u200B        - арматура: ${entity.armature.label}`,
                        { text: 'длина: ' + entity.armatureLen.toFixed(2) + ' м', alignment: 'right' },
                        { text: formatPrice(entity.armaturePrice), alignment: 'right' }, '']);
                    rows.push([{ text: 'Стоимость плиты: ' +  formatPrice(entity.price), colSpan: 4, alignment: 'right' }, {}, {}, {}]);
                    break;
                }
                default:
                    rows.push([entity.definition.name, ' ', ' ', formatPrice(entity.price)]);
            }
        });
        rows.push([{ text: 'Итого: ' + formatPrice(layerprice), colSpan: 4, alignment: 'right' }, {}, {}, {}]);
        rows.push([{ text: ' ', colSpan: 4, alignment: 'left' }, {}, {}, {}]);
        fullprice += layerprice;

        console.log(layer);
    });
    rows.push([{ text: 'Итого за весь дом: ' + formatPrice(fullprice), colSpan: 4, alignment: 'right' }, {}, {}, {}]);

    console.log('layers', layers);

    const docDefinition = {
        pageSize: 'A4',
        pageMargins: [20, 20, 20, 20],
        content: [
            {
                image: window.img,
                width: 380,
                alignment: 'center'
            },
            ' ',
            {
                table: {
                    widths: ['*', 'auto', 'auto', 'auto'],
                    body: rows,
                    headerRows: 0
                }
            }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 16,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                margin: [0, 5, 0, 15]
            },
            tableHeader: {
                bold: true,
                fontSize: 10,
                color: 'black'
            },
            entity: {
                bold: true,
                fontSize: 8,
                color: 'black'
            }
        },
        defaultStyle: {
            fontSize: 8
        },
        footer: (currentPage, pageCount) => {
            return { text: currentPage.toString() + ' of ' + pageCount, alignment: 'center' };
        },
        background: 'http://cottegebuild.me/'
    };

    pdfMake.createPdf(docDefinition).download('a4.pdf');
};

export default () => {
    sketchup.getView({
        onCompleted: () => {
            console.log('Ruby side done.');
            genDoc();
        }
    });
};
