
import { combineReducers } from 'redux';
import selection from './selection';
import model from './model';

// объединяем в кучу
export default combineReducers({
    selection,
    model
});
