
export const SET_SELECTION = 'SET_SELECTION';
export const SET_ACTIVEPATH = 'SET_ACTIVEPATH';

export default (state = { selection: [], activePath: [] }, action) => {
    switch (action.type) {
        case SET_SELECTION: {
            const newState = Object.assign({}, state, { selection: action.selection });

            return newState;
        }
        case SET_ACTIVEPATH: {
            const newState = Object.assign({}, state, { activePath: action.activePath });

            return newState;
        }
        default:
            return state;
    }
};
