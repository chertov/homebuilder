
export const SET_MODEL = 'SET_MODEL';

export default (state = null, action) => {
    switch (action.type) {
        case SET_MODEL: {
            return Object.assign({}, action.model);
        }
        default:
            return state;
    }
};
