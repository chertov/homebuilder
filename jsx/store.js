
import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducers from './reducers';
import rootSaga from './sagas';

/* global window */

let store = null;

const getStore = (initState = null) => {
    if (store !== null) { return store; }

    const stateValue = Object.assign({}, initState);

    const sagaMiddleware = createSagaMiddleware();
    const composes = compose(
        applyMiddleware(sagaMiddleware),
        // If you are using the devToolsExtension, you can add it here also
        window.devToolsExtension ? window.devToolsExtension() : f => f,
    );

    store = createStore(reducers, stateValue, composes);
    sagaMiddleware.run(rootSaga);
    return store;
};

export default getStore;
