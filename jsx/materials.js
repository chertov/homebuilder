
import React from 'react';

export const notset = 'notset';
export const NotSetOption = <option key={notset} value={notset}>Not set</option>;
export const ObjectTypes = {
    wire: 'Wire material',
    plane: 'Plane material',
    volume: 'Volume material',
    fixprice: 'Fix price',
    plate: 'Plate'
};

export const armatures = {
    a08: { label: 'А500С ф8 АIII', price: 15, weight: 1 },
    a10: { label: 'А500С ф10 АIII', price: 20.5, weight: 1 },
    a12: { label: 'А500С ф12 АIII', price: 28, weight: 1 },
    a14: { label: 'А500С ф14 АIII', price: 40, weight: 1 },
    a16: { label: 'А500С ф16 АIII', price: 49, weight: 1 },
    a18: { label: 'А500С ф18 АIII', price: 69, weight: 1 }
};
export const wirematerials = Object.assign(armatures);

export const windows = {
    panoram: { label: 'Панорамное глухое двухкамерное', price: 3000, weight: 10 },
    rehau: { label: 'Rehau двухкамерное', price: 6500, weight: 1 },
    stairs: { label: 'Лестница', price: 5500, weight: 100 },
    roof: { label: 'Крыша', price: 1500, weight: 20 },
    geo: { label: 'Геотекстиль', price: 2000 / 50.0, weight: 1 } // 2000р за рулон 50 м2 площади
};
export const planematerials = Object.assign({
    гидроизоляция: { label: 'Гидроизоляция', price: 3186 / 20.0, weight: 1 }
}, windows);

export const dostavka = 600;
export const concretes = { // стоимость + доставка
    m200: { label: 'М200', price: 3650 + dostavka, weight: 2700 },
    m300: { label: 'М300', price: 3900 + dostavka, weight: 2700 },
    m500: { label: 'М500', price: 4500 + dostavka, weight: 2700 },
    m600: { label: 'М600 мостовой', price: 5100 + dostavka, weight: 2700 }
};
export const volumematerials = Object.assign({
    песок: { label: 'Песок', price: 600, weight: 1440 },
    щебень: { label: 'Щебень', price: 1600, weight: 1600 },
    пеноблок_d400: { label: 'Пеноблок D400', price: 4500, weight: 400 },
    пеноблок_d500: { label: 'Пеноблок D500', price: 4500, weight: 500 },
    пеноблок_d600: { label: 'Пеноблок D600', price: 4500, weight: 600 },
    ekskavator: { label: 'Рытье котлована', price: 80, weight: 0 },
    ekskavator_delivery: { label: 'Рытье котлована с вывозом', price: 300, weight: 0 }
}, concretes);

export const defaultMaterial = { id: notset, price: 0, weight: 0 };
export const getMaterialById = id => {
    if (!id) { return defaultMaterial; }
    if (id === notset) { return defaultMaterial; }
    if (wirematerials[id]) { return Object.assign({ id }, wirematerials[id]); }
    if (planematerials[id]) { return Object.assign({ id }, planematerials[id]); }
    if (volumematerials[id]) { return Object.assign({ id }, volumematerials[id]); }
    return defaultMaterial;
};
export const getMaterial = entity => {
    let id = notset;

    try {
        if (entity.attributes.materialId) { id = entity.attributes.materialId; }
    } catch (e) {
        return defaultMaterial;
    }
    return getMaterialById(id);
};
