
export const formatPrice = num => {
    const p = (num).toFixed(2).split('.');
    const str = p[0].split('').reverse().reduce((acc, num, i, orig) => {
        return num === '-' ? acc : num + (i && !(i % 3) ? "'" : '') + acc;
    }, '');

    // str += '.' + p[1];
    return str + ' ₽';
};

export const calcArea = entity => {
    let entities = [];

    try { if (entity.definition.entities) { entities = entity.definition.entities; } } catch (e) { return 0; }
    return entities.reduce((sum, entity) => {
        if (entity && entity.class === 'Face') { return sum + entity.area; }
        return sum;
    }, 0);
};

export const calcLength = entity => {
    let entities = [];

    try { if (entity.definition.entities) { entities = entity.definition.entities; } } catch (e) { return 0; }
    return entities.reduce((sum, entity) => {
        if (entity && entity.class === 'Edge') { return sum + entity.length; }
        return sum;
    }, 0);
};

// ucs-2 string to base64 encoded ascii
export function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}
// base64 encoded ascii to ucs-2 string
export function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
// ucs-2 string to base64 encoded ascii
export function objToBase64(obj) {
    return window.btoa(unescape(encodeURIComponent(JSON.stringify(obj))));
}
// base64 encoded ascii to ucs-2 string
export function base64ToObj(str) {
    return JSON.parse(decodeURIComponent(escape(window.atob(str))));
}
