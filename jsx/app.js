
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import getStore from './store';
import router from './router.jsx';
import './callbacks';

const pathname = window.location.pathname;
const url = window.location.href;

console.log('url', pathname, url);

/* global document */
document.addEventListener('DOMContentLoaded', event => {
    const store = getStore();

    sketchup.reloadAll();

    ReactDOM.render(
        <Provider store={store}>
            {router}
        </Provider>,
        document.getElementById('root')
    );
});
