
/* global window */
/* global document */
/* global console */

import { setSelection, setActivePath } from 'api/selection';
import { setModel } from 'api/model';

window.updateModel = modelJson => {
    const model = JSON.parse(modelJson);

    setModel(model);
    setSelection(model.selection.map(entity => entity.entityID));
    setActivePath(model.activePath.map(entity => entity.entityID));
};

window.onActiveSectionPlaneChanged = entitiesJson => {
    const entities = JSON.parse(entitiesJson);

    console.log('onActiveSectionPlaneChanged', entities);
};
window.onElementAdded = (entitiesJson, entityJson) => {
    const entities = JSON.parse(entitiesJson);
    const entity = JSON.parse(entityJson);

    console.log('onElementAdded', entities, entity);
};
window.onElementModified = (entitiesJson, entityJson) => {
    const entities = JSON.parse(entitiesJson);
    const entity = JSON.parse(entityJson);

    console.log('onElementModified', entities, entity);
};
window.onElementRemoved = (entitiesJson, entityId) => {
    const entities = JSON.parse(entitiesJson);

    console.log('onElementRemoved', entities, entityId);
};
window.onEraseEntities = entitiesJson => {
    const entities = JSON.parse(entitiesJson);

    console.log('onEraseEntities', entities);
};

window.onSelectionAdded = (selectionJson, entityJson) => {
    // const selection = JSON.parse(selectionJson);
    // const entity = JSON.parse(entityJson);

    // console.log('onSelectionAdded', selection, entity);
};
window.onSelectionBulkChange = selectionJson => {
    const selection = JSON.parse(selectionJson);

    setSelection(selection.map(entity => entity.entityID));
    // console.log('onSelectionBulkChange', selection);
};
window.onSelectionCleared = selectionJson => {
    const selection = JSON.parse(selectionJson);

    setSelection([]);
    // console.log('onSelectionCleared', selection);
};
window.onSelectionRemoved = (selectionJson, entityJson) => {
    // const selection = JSON.parse(selectionJson);
    // const entity = JSON.parse(entityJson);

    // console.log('onSelectionRemoved', selection, entity);
};

window.onActivePathChanged = modelJson => {
    const model = JSON.parse(modelJson);

    setActivePath(model.activePath.map(entity => entity.entityID));
    console.log('onActivePathChanged', model);
};
window.onPlaceComponent = instanceJson => {
    const instance = JSON.parse(instanceJson);

    console.log('onPlaceComponent', instance);
};

window.loadImage = b64imgData => {
    window.img = b64imgData;
};
